#include "qwavescene.h"
#include <QGraphicsItem>

QWaveScene::QWaveScene( QObject *parent ) :
	QGraphicsScene( parent )
{
	setup();
}

QWaveScene::QWaveScene( qreal width, qreal height, QObject *parent ) :
	QGraphicsScene( 0, 0, width, height, parent )
{
	setup();
}

void QWaveScene::setup( void )
{
	this->setBackgroundBrush( Qt::black );
	m_marginLeft = 60;
	m_marginRight = 10;
	m_marginTop = 10;
	m_marginButton = 60;
	setGridPen();
	setWavePen(); 
	setScope( 10, 10 );
}
void QWaveScene::setGridPen( void )
{
	m_gridPen.setColor( Qt::gray );
	m_gridPen.setStyle( Qt::DashLine );
	m_gridPen.setWidthF( 1 );

	m_gridRectPen.setColor( Qt::gray );
	m_gridRectPen.setStyle( Qt::SolidLine );
	m_gridRectPen.setWidthF( 3 );

}

void QWaveScene::setWavePen( void )
{
	m_wavePen.setColor( Qt::green );
	m_wavePen.setStyle( Qt::SolidLine );
	m_wavePen.setWidthF( 1 );
}

QRectF QWaveScene::mainRect( void )
{
	QRectF rect;
	rect.setRect( sceneRect().x() + m_marginLeft, sceneRect().y() + m_marginTop,
			sceneRect().width() - m_marginRight - m_marginLeft, sceneRect().height() - m_marginTop - m_marginButton );
	return rect;
}

void QWaveScene::updateScale( void )
{
	/* Scope of X value, from 0 to +scopeX */
	/* Scope of Y value, from -scopeY to +scopeY */
	m_scaleX = mainRect().width() / m_scopeX;
	m_scaleY = mainRect().height() / m_scopeY / 2;
}

void QWaveScene::resize( qreal width, qreal height )
{
	setSceneRect( 0, 0, width, height );
	updateScale( );
	reDraw();
}

QGraphicsSimpleTextItem *QWaveScene::addText( qreal x, qreal y, QString text, TextAlign_t align, bool isVertical )
{
	QFont serifFont("Times", 14, QFont::Bold);
	QGraphicsSimpleTextItem *textItem = new QGraphicsSimpleTextItem( text );
	textItem->setFont( serifFont );
	textItem->setBrush( QColor(255, 255, 0, 127) ); 
	QRectF rect = textItem->boundingRect();
	switch ( align ) {
		case TextAlignTopLeft:
			textItem->setPos( x, y );
			break;
		case TextAlignTopCenter:
			textItem->setPos( x - rect.width() / 2, y);
			break;
		case TextAlignTopRight:
			textItem->setPos( x - rect.width(), y);
			break;
		case TextAlignCenterLeft:
			textItem->setPos( x, y - rect.height() / 2);
			break;
		case TextAlignCenter:
			textItem->setPos( x - rect.width() / 2, y - rect.height() / 2);
			break;
		case TextAlignCenterRight:
			textItem->setPos( x - rect.width(), y - rect.height() / 2);
			break;
		case TextAlignButtomLeft:
			textItem->setPos( x, y - rect.height() );
			break;
		case TextAlignButtomCenter:
			textItem->setPos( x - rect.width() / 2, y - rect.height() );
			break;
		case TextAlignButtomRight:
			textItem->setPos( x - rect.width(), y - rect.height() );
			break;
		default:
			textItem->setPos( x, y );
			break;
	}

	if ( isVertical )
		textItem->setRotation ( -90 );
	this->addItem( textItem );
	return textItem;
}

void QWaveScene::showGrid( void )
{
	showGrid( 10, 10 );
}

void QWaveScene::showGrid( int numX, int numY )
{
	removeGrid();
	QRectF rect = mainRect();
	m_gridLinesList.push_back( ( QGraphicsItem * )this->addRect( rect, m_gridRectPen ) );

	QPointF p1;
	QPointF p2;
	int mainRectLineWidth = 4;

	m_gridLinesList.push_back( addText( m_marginLeft / 2, rect.height() / 2, "Voltage", TextAlignCenter, true ) );
	m_gridLinesList.push_back( addText( rect.x() - mainRectLineWidth, rect.y(), QString::number( m_scopeY ), TextAlignCenterRight ) );
	//绘制横向刻度线, voltage
	for ( int i = 1; i < numX; i++ ) {
		p1.setX( rect.x() );
		p1.setY( rect.y() + rect.height() / numX * i );
		p2.setX( rect.x() + rect.width() );
		p2.setY( rect.y() + rect.height() / numX * i );
		m_gridLinesList.push_back( ( QGraphicsItem* ) this->addLine( QLineF( p1, p2 ), m_gridPen) );
		m_gridLinesList.push_back( addText( p1.x() - mainRectLineWidth, p1.y(), QString::number( m_scopeY - i * ( m_scopeY * 2 / numY ) ), TextAlignCenterRight ) );
	}

	m_gridLinesList.push_back( addText( rect.width() / 2, rect.height() + m_marginButton / 2, "Time", TextAlignCenter ) );
	m_gridLinesList.push_back( addText( rect.x(), rect.y() + rect.height() + mainRectLineWidth, "0", TextAlignTopCenter ) );
	//绘制纵向刻度线, time
	for ( int i = 1; i < numY; i++ ) {
		p1.setX( rect.x() + rect.width() / numY * i );
		p1.setY( rect.y() );
		p2.setX( rect.x() + rect.width() / numY * i );
		p2.setY( rect.y() + rect.height() );
		m_gridLinesList.push_back( ( QGraphicsItem* ) this->addLine( QLineF( p1, p2 ), m_gridPen) );
		m_gridLinesList.push_back( addText( p2.x(), p2.y() + mainRectLineWidth, QString::number( i * ( m_scopeX / numX ) ), TextAlignTopCenter ) );
	}
}

void QWaveScene::hideGrid( void )
{
}

void QWaveScene::removeGrid( void )
{
	if ( m_gridLinesList.isEmpty() )
		return;
	foreach( QGraphicsItem *item, m_gridLinesList )
	{
		this->removeItem( item );
	}
	m_gridLinesList.clear();
}

int QWaveScene::drawWave( unsigned int channel, qreal step, QList<qreal> waveData, bool needUpdateData )
{

	int status = 0;
	QRectF rect = mainRect();

	if ( needUpdateData ) {
		m_currentChannel = channel;
		m_stepDB[ channel ] = step;
		m_waveDataDB[ channel ] = waveData;
	}
	if ( rect.width() <=0 || rect.height() <= 0 || waveData.isEmpty() )
		return -1;

	qreal maxValue = waveData[0];//waveData中的最大值
	for ( int i = 0; i < waveData.size(); i++ ) {
		if ( waveData[i] > maxValue )
			maxValue = waveData[i];
	}
	if (step * waveData.size() > rect.width()) {
		status = 1;//横向显示超出范围
	}
	if (maxValue > rect.height()) {
		status = 2;//纵向显示超过范围
	}
	if ((step * waveData.size() > rect.width()) && (maxValue > rect.height())) {
		status = 3;//横向和纵向的显示都超出了范围
	}

	QPointF p1;
	QPointF p2;
	p1.setX( rect.x() );
	p1.setY( rect.y() + rect.height() / 2 - waveData[0] * m_scaleY );

	qreal numOfDataPerPixel = waveData.size() / rect.width();
	for ( int i = 1; i < waveData.size(); ) {
		p2.setX( rect.x() + (i * step) * m_scaleX );
		p2.setY( rect.y() + rect.height() / 2 - waveData[i] * m_scaleY );
		m_waveLinesList.push_back( ( QGraphicsItem * )this->addLine( QLineF( p1, p2 ), m_wavePen ) );
		p1 = p2;

		/* if number of data is more than the width(pixels) of the scene, NOT draw every data to save time */
		if ( numOfDataPerPixel >= 1 )
			i += numOfDataPerPixel;
		else
			i++;
	}

	return status;
}

void QWaveScene::removeWave( void )
{
	if ( m_waveLinesList.isEmpty() )
		return;
	foreach( QGraphicsItem *item, m_waveLinesList )
	{
		this->removeItem( item );
	}
	m_waveLinesList.clear();
}

void QWaveScene::reDraw( void )
{
	removeGrid();
	showGrid();

	removeWave();
	drawWave( m_currentChannel, m_stepDB[ m_currentChannel ], m_waveDataDB[ m_currentChannel ], false );
}

void QWaveScene::reDraw( unsigned int channel )
{
	removeGrid();
	showGrid();

	removeWave();
	drawWave( channel, m_stepDB[ channel ], m_waveDataDB[ channel ], false );
}
