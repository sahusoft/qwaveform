#-------------------------------------------------
#
# Project created by QtCreator 2014-04-03T22:00:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QWaveform
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qwavescene.cpp \
    sqdial.cpp

HEADERS  += mainwindow.h \
    qwaveform.h \
    qwavescene.h \
    sqdial.h

FORMS    += mainwindow.ui
