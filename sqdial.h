#ifndef SQDIAL_H
#define SQDIAL_H

#include <QDial>
#include <QPair>

class SQDial : public QDial
{
    Q_OBJECT
public:
    explicit SQDial( QWidget *parent = 0);
    explicit SQDial( QDial *copy );
		void setNotchList( QList< QPair<QString, qreal> > &notchList ) { m_notchList = notchList; }
		inline qreal getNotchValue( void ) { return getNotchValueAt( this->value() ); }
		inline qreal getNotchValueAt( int index ) {
			if ( index >= 0 && index < m_notchList.size() )
				return m_notchList[ index ].second;
			else
				return 0;
		}
		inline QString& getNotchTextAt( int index ) {
			if ( index >= 0 && index < m_notchList.size() )
				return m_notchList[ index ].first;
			else
				return m_errText;
		}
protected:
		void paintEvent( QPaintEvent *event );
private:
		QList< QPair<QString, qreal> > m_notchList;
		QString m_errText;

signals:

public slots:

};

#endif // SQDIAL_H
