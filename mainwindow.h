#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qwavescene.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
		void resizeEvent(QResizeEvent *);
		void updateWidgets( void );
		void test( void );
private slots:
		void setVoltageScope( int index );
		void setTimeScope( int index );
		void setChannel( int index );

        void on_quitPushButton_clicked();

private:
    Ui::MainWindow *ui;
		QWaveScene *m_scene;
};

#endif // MAINWINDOW_H
