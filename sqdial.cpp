#include <QVariant>
#include <QMetaProperty>
#include <QPainter>
#include <math.h>
#include "sqdial.h"

SQDial::SQDial( QWidget *parent ) :
	QDial( parent )
{
}

SQDial::SQDial( QDial *copy ) :
	QDial( ( QWidget * )copy->parent() )
{
	const QMetaObject *metaobject = copy->metaObject();
	int count = metaobject->propertyCount();
	for (int i=0; i<count; ++i) {
		QMetaProperty metaproperty = metaobject->property(i);
		const char *name = metaproperty.name();
		if ( strcmp( name, "objectName") == 0 )
			continue;
		QVariant value = copy->property(name);
		this->setProperty( name, value);
	}
}

void SQDial::paintEvent( QPaintEvent *event )
{
	QDial::paintEvent( event );
	QPainter painter(this);
	QRect rect = this->rect();
	painter.setPen( QPen( palette().color( QPalette::WindowText ) ) );
	int x0 = rect.width() / 2 - 5;
	int y0 = rect.height() / 2 + 5;
	painter.drawText( x0, y0, "x" );

	qreal r = rect.width() / 2;
	qreal pi = 3.1415926;
	qreal d2a = pi / 180;
	qreal degreeDelta = ( 360 - 60 ) / ( this->maximum() - this->minimum() );

	for ( int i = 0; i <= this->maximum(); i += this->singleStep() ) {
		int x = x0 + sin ( ( 30 + degreeDelta * i ) * d2a ) * r;
		int y = y0 + cos ( ( 30 + degreeDelta * i ) * d2a ) * r;
		//painter.drawText( x, y, QString::number( this->maximum() - i ) );
		int index = this->maximum() - this->minimum() - i;
		if ( index >=0 && index < m_notchList.size() )
			painter.drawText( x, y, m_notchList[ index ].first );
	}

	painter.end();
}
