#include <math.h>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "SQDial.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	//m_scene = new QWaveScene( 100, 200, this );
	m_scene = new QWaveScene( this );
	ui->waveView->setScene( m_scene );

	updateWidgets();
	
	test();
}

void MainWindow::updateWidgets( void )
{
	SQDial *voltageDial = new SQDial( ui->voltageDial );
	QList< QPair<QString, qreal> > voltageNotchList;
	voltageNotchList.append( qMakePair( QString("10mv"), qreal(10 * 0.001) ) );
	voltageNotchList.append( qMakePair( QString("20mv"), qreal(20 * 0.001) ) );
	voltageNotchList.append( qMakePair( QString("50mv"), qreal(50 * 0.001) ) );
	voltageNotchList.append( qMakePair( QString("100mv"), qreal(100 * 0.001) ) );
	voltageNotchList.append( qMakePair( QString("200mv"), qreal(200 * 0.001) ) );
	voltageNotchList.append( qMakePair( QString("500mv"), qreal(500 * 0.001) ) );
	voltageNotchList.append( qMakePair( QString("1v"), qreal(1) ) );
	voltageNotchList.append( qMakePair( QString("2v"), qreal(2) ) );
	voltageNotchList.append( qMakePair( QString("5v"), qreal(5) ) );
	voltageDial->setNotchList( voltageNotchList );
    ui->verticalLayout_5->replaceWidget( ui->voltageDial, voltageDial );
    //ui->widget->layout()->replaceWidget( ui->voltageDial, voltageDial );
	voltageDial->show();
	delete ui->voltageDial;
	ui->voltageDial = voltageDial;

	SQDial *channelDial = new SQDial( ui->channelDial );
	QList< QPair<QString, qreal> > channelNotchList;
	channelNotchList.append( qMakePair( QString("Channel 1"), qreal(1) ) );
	channelNotchList.append( qMakePair( QString("Channel 2"), qreal(2) ) );
	channelNotchList.append( qMakePair( QString("Channel 3"), qreal(3) ) );
	channelNotchList.append( qMakePair( QString("Channel 4"), qreal(4) ) );
	channelDial->setNotchList( channelNotchList );
	ui->verticalLayout_7->replaceWidget( ui->channelDial, channelDial );
	channelDial->show();
	delete ui->channelDial;
	ui->channelDial = channelDial;

	SQDial *timeDial = new SQDial( ui->timeDial );
	QList< QPair<QString, qreal> > timeNotchList;
	timeNotchList.append( qMakePair( QString("1us"), qreal(1e-6) ) );
	timeNotchList.append( qMakePair( QString("2us"), qreal(2e-6) ) );
	timeNotchList.append( qMakePair( QString("5us"), qreal(5e-6) ) );
	timeNotchList.append( qMakePair( QString("10us"), qreal(10e-6) ) );
	timeDial->setNotchList( timeNotchList );
	ui->verticalLayout_6->replaceWidget( ui->timeDial, timeDial );
	timeDial->show();
	delete ui->timeDial;
	ui->timeDial = timeDial;

	connect( ui->voltageDial, SIGNAL( valueChanged(int) ), this, SLOT( setVoltageScope(int) ) );
	connect( ui->channelDial, SIGNAL( valueChanged(int) ), this, SLOT( setChannel(int) ) );
	connect( timeDial, SIGNAL( valueChanged(int) ), this, SLOT( setTimeScope(int) ) );

	setVoltageScope( ui->voltageDial->value() );
	setChannel( ui->channelDial->value() );
	setTimeScope( ui->timeDial->value() );
}

MainWindow::~MainWindow()
{
	delete ui;
	if ( m_scene )
		delete m_scene;
}

void MainWindow::resizeEvent(QResizeEvent *)
{
	int viewW = ui->waveView->width();
	int viewH = ui->waveView->height();
	//m_scene->setScope( viewW, viewH );
	m_scene->resize( viewW, viewH );
}

void MainWindow::setVoltageScope( int index )
{
	SQDial *voltageDial = dynamic_cast< SQDial *>( ui->voltageDial );
    if ( voltageDial ) {
		m_scene->setScopeY( voltageDial->getNotchValueAt( index ) );
        ui->voltageLineEdit->setText( voltageDial->getNotchTextAt( index ) );
    }
}

void MainWindow::setTimeScope( int index )
{
	SQDial *timeDial = dynamic_cast< SQDial *>( ui->timeDial );
	if ( timeDial ) {
		m_scene->setScopeX( timeDial->getNotchValueAt( index ) );
		ui->timeLineEdit->setText( timeDial->getNotchTextAt( index ) );
	}
}

void MainWindow::setChannel( int index )
{
	SQDial *channelDial = dynamic_cast< SQDial *>( ui->channelDial );		
	if ( channelDial ) {
		m_scene->setChannel( channelDial->getNotchValueAt( index ) );
		ui->channelLineEdit->setText( channelDial->getNotchTextAt( index ) );
	}
}

void MainWindow::test( void )
{
	QList<qreal> waveData;
	qreal pi = 3.1415926;
	SQDial *voltageDial = dynamic_cast< SQDial *>( ui->voltageDial );
	SQDial *timeDial = dynamic_cast< SQDial *>( ui->timeDial );
	SQDial *channelDial = dynamic_cast< SQDial *>( ui->channelDial );
	if ( voltageDial == NULL || timeDial == NULL || channelDial == NULL )
		return;
	qreal voltageScope = voltageDial->getNotchValue();
	qreal timeScope = timeDial->getNotchValue();
	unsigned int channel = channelDial->getNotchValue();

	qreal w = 1e7;
	qreal cycle = 2 * pi / w;
	qreal step = cycle / 100;
	qreal during = 3 * cycle;

	m_scene->setScope( timeScope, voltageScope );
	for ( qreal i = 0; i <= during; i=i+step ) {
		waveData.append( voltageScope * sin( w * i ) );
	}
	m_scene->drawWave( 1, step, waveData );

	waveData.clear();
	for ( qreal i = 0; i <= during; i=i+step ) {
		waveData.append( voltageScope * cos( w * i ) );
	}
	m_scene->drawWave( 2, step, waveData );

	waveData.clear();
	for ( qreal i = 0; i <= during; i=i+step ) {
		waveData.append( voltageScope * ( sqrt( 1 + sin( w * i ) ) + sqrt( 1 - sin( w * i ) ) ) - voltageScope );
	}
	m_scene->drawWave( 3, step, waveData );

	waveData.clear();
	for ( qreal i = 0; i <= during; i=i+step ) {
		waveData.append( voltageScope * ( ( (int)( i / step ) % 20 ) - 10 ) / 10 );
	}
	m_scene->drawWave( 4, step, waveData );

	m_scene->setChannel( channel );
}

void MainWindow::on_quitPushButton_clicked()
{
    QApplication::quit();
}
