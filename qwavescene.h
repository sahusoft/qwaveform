#ifndef QWAVESCENE_H
#define QWAVESCENE_H

#include <QGraphicsScene>
#include <QPen>
#include <QMap>

class QWaveScene : public QGraphicsScene
{
	public:
		typedef enum TextAlign {
			TextAlignTopLeft,
			TextAlignTopCenter,
			TextAlignTopRight,
			TextAlignCenterLeft,
			TextAlignCenter,
			TextAlignCenterRight,
			TextAlignButtomLeft,
			TextAlignButtomCenter,
			TextAlignButtomRight,
		} TextAlign_t;
public:
    QWaveScene( QObject *parent = 0 );
    QWaveScene( qreal width, qreal height, QObject *parent = 0 );
		void showGrid( void );
		void showGrid( int numX, int numY );
		void hideGrid( void );
		void removeGrid( void );
		void setScope( void );
		void setScopeX( qreal scopeX ) { m_scopeX = scopeX; updateScale(); reDraw(); }
		void setScopeY( qreal scopeY ) { m_scopeY = scopeY; updateScale(); reDraw(); }
		void setScope( qreal scopeX, qreal scopeY ) { setScopeX( scopeX ); setScopeY( scopeY ); }
		void updateScale( void );
		void resize( qreal width, qreal height );
		int drawWave( unsigned int channel, qreal step, QList<qreal> waveData, bool needUpdateData = true );
		void removeWave( void );
		void reDraw( void );
		void reDraw( unsigned int channel );
		void setChannel( unsigned int channel ) { m_currentChannel = channel; reDraw( channel ); }
		QGraphicsSimpleTextItem *addText( qreal x, qreal y, QString text, TextAlign_t align, bool isVertical = false );
private:
		void setup( void );
		void setGridPen( void );
		void setWavePen( void );
		QRectF mainRect( void );
private:
		QPen m_gridPen;
		QPen m_gridRectPen;
		QPen m_wavePen;
		qreal m_scopeX;
		qreal m_scopeY;
		qreal m_scaleX;
		qreal m_scaleY;
		qreal m_marginRight;
		qreal m_marginLeft;
		qreal m_marginTop;
		qreal m_marginButton;

		QSize m_sceneSize;

		QList<QGraphicsItem*> m_gridLinesList;
		QList<QGraphicsItem*> m_waveLinesList;
		unsigned int m_currentChannel;
		QMap< unsigned int, QList<qreal> > m_waveDataDB;
		QMap< unsigned int, qreal > m_stepDB;
};

#endif // QWAVESCENE_H
